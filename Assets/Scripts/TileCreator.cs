﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyPlugins
{
    public class TileCreator : MonoBehaviour
    {
        

        public GameObject prefab;
        int tilecount=1;
        
        

        void OnDrawGizmos()
        {
            Vector3 pos = transform.position;
            //pos.y += size * 0.5f; // size >> 1

            Vector3 size = new Vector3(1, 0, 1);

            Gizmos.DrawWireCube(pos, size);

        }

        public void CreateTile()
        {
            GameObject clone = Instantiate(prefab,transform.position,Quaternion.identity);
            clone.name = prefab.name + "-" + tilecount;
            tilecount++;
            

        }

        public void CreateLeft()
        {

            transform.position = new Vector3(transform.position.x+1, 0, transform.position.z);
            GameObject clone = Instantiate(prefab, transform.position, Quaternion.identity);
            clone.name = prefab.name + "-" + tilecount;
            tilecount++;

        }

        public void CreateRight()
        {

            transform.position = new Vector3(transform.position.x - 1, 0, transform.position.z);
            GameObject clone = Instantiate(prefab, transform.position, Quaternion.identity);
            clone.name = prefab.name + "-" + tilecount;
            tilecount++;

        }

        public void CreateUp()
        {

            transform.position = new Vector3(transform.position.x, 0, transform.position.z-1);
            GameObject clone = Instantiate(prefab, transform.position, Quaternion.identity);
            clone.name = prefab.name + "-" + tilecount;
            tilecount++;

        }

        public void CreateDown()
        {

            transform.position = new Vector3(transform.position.x, 0, transform.position.z+1);
            GameObject clone = Instantiate(prefab, transform.position, Quaternion.identity);
            clone.name = prefab.name + "-" + tilecount;
            tilecount++;

        }

        public void ResetCount()
        {
            tilecount = 1;
        }


       
    }
}