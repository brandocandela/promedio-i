﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MyPlugins
{
    
    [CustomEditor(typeof(TileCreator))]
    public class TileCreatorEditor : Editor
    {
        public bool mid = true;
        public bool left = false;
        public bool right = false;
        public bool top = false;
        public bool down = false;
        public bool reset = false;



        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            TileCreator myTarget = (TileCreator)target;
            GUI.enabled = mid;
            if (GUILayout.Button("Create Tile"))
            {
                myTarget.CreateTile();
                mid = false;
                left = true;
                right = true;
                top = true;
                down = true;
                reset = true;
            }

            GUI.enabled = left;
            if (GUILayout.Button("Create Left"))
            {
                myTarget.CreateLeft();
                mid = false;
                left = true;
                right = false;
                top = true;
                down = true;
                reset = true;
            }

            GUI.enabled = right;
            if (GUILayout.Button("Create Right"))
            {
                myTarget.CreateRight();
                mid = false;
                left = false;
                right = true;
                top = true;
                down = true;
                reset = true;
            }

            GUI.enabled = top;
            if (GUILayout.Button("Create Top"))
            {
                myTarget.CreateUp();
                mid = false;
                left = true;
                right = true;
                top = true;
                down = false;
                reset = true;
            }

            GUI.enabled = down;
            if (GUILayout.Button("Create Down"))
            {
                myTarget.CreateDown();
                mid = false;
                left = true;
                right = true;
                top = false;
                down = true;
                reset = true;
            }

            GUI.enabled = reset;
            if (GUILayout.Button("Reset Count"))
            {
                myTarget.ResetCount();              
                reset = false;
            }
        }


    }
}

   